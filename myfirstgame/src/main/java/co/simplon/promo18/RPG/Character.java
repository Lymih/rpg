package co.simplon.promo18.RPG;



public class Character {

  private String charType;
  private int charHp;
  private int charMaxHp;
  private int charLvl;
  private int charAtk;
  private int charDef;

  public Character(String charType, int charHp, int charMaxHp, int charLvl, int charAtk, int charDef) {
    this.charType = charType;
    this.charHp = charHp;
    this.charMaxHp = charMaxHp;
    this.charLvl = charLvl;
    this.charAtk = charAtk;
    this.charDef = charDef;
    
  }

 

  public String getCharType() {
    return charType;
  }

  public void setCharType(String charType) {
    this.charType = charType;
  }

  public int getCharHp() {
    return charHp;
  }

  public void setCharHp(int charHp) {
    this.charHp = charHp;
  }
  public int getCharMaxHp() {
    return charMaxHp;
  }

  public void setCharMaxHp(int charMaxHp) {
    this.charMaxHp = charMaxHp;
  }
  public int getCharLvl() {
    return charLvl;
  }

  public void setCharLvl(int charLvl) {
    this.charLvl = charLvl;
  }

  public int getCharAtk() {
    return charAtk;
  }

  public void setCharAtk(int charAtk) {
    this.charAtk = charAtk;
  }

  public int getCharDef() {
    return charDef;
  }

  public void setCharDef(int charDef) {
    this.charDef = charDef;
  }

 

}
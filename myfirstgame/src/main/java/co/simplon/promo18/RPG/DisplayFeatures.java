package co.simplon.promo18.RPG;



public class DisplayFeatures {
  

  public String colorReset = "\u001B[0m";
  public String colorBlack = "\u001B[30m";
  public String colorRed = "\u001B[31m";
  public String colorGreen = "\u001B[32m";
  public String colorYellow = "\u001B[33m";
  public String colorBlue = "\u001B[34m";
  public String colorPurple = "\u001B[35m";
  public String colorCyan = "\u001B[36m";
  public String colorWhite = "\u001B[37m";

  public String colorBlackBold = "\033[1;30m";
  public String colorRedBold = "\033[1;31m";
  public String colorGreenBold = "\033[1;32m";
  public String colorYellowBold = "\033[1;33m";
  public String colorBlueBold = "\033[1;34m";
  public String colorPurpleBold = "\033[1;35m";
  public String colorCyanBlod = "\033[1;36m";
  public String colorWhiteBold = "\033[1;37m";

  Input input = new Input();

  public void addDelay(int delay) {
    try {

      Thread.sleep(delay);
    } catch (InterruptedException e) {

      // gestion de l'erreur
    }
  }

  public void printPoints(int delay) {
    for (int i = 0; i < 3; i++) {
      System.out.println(".");
      addDelay(delay);
    }
  }

  public void pressToContinue() {
    System.out.println("Press Enter to continue");
    try {
      System.in.read();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public char askYesOrNo() {
    System.out.println("y - Yes         n - No");
    return input.scanChar();
  }
  public void displayReturnMainMenu(){  
    System.out.print("Return to main menu");
    printPoints(500);
  }

  public void colorText(String text, String color, Boolean newLine) {
    switch (color) {
      case "black":
        System.out.print(colorBlack + text + colorReset);
        break;
      case "red":
        System.out.print(colorRed + text + colorReset);
        break;
      case "green":
        System.out.print(colorGreen + text + colorReset);
        break;
      case "yellow":
        System.out.print(colorYellow + text + colorReset);
        break;
      case "white":
        System.out.print(colorWhite + text + colorReset);
      case "blue":
        System.out.print(colorBlue + text + colorReset);
        break;
      case "cyan":
        System.out.print(colorCyan + text + colorReset);
        break;
      case "purple":
        System.out.print(colorPurple + text + colorReset);
        break;
      case "blackBold":
        System.out.print(colorBlack + text + colorReset);
        break;
      case "redBold":
        System.out.print(colorRed + text + colorReset);
        break;
      case "greenBold":
        System.out.print(colorGreenBold + text + colorReset);
        break;
      case "yellowBold":
        System.out.print(colorYellowBold + text + colorReset);
        break;
      case "whiteBold":
        System.out.print(colorWhiteBold + text + colorReset);
      case "blueBold":
        System.out.print(colorBlueBold + text + colorReset);
        break;
      case "cyanBold":
        System.out.print(colorCyanBlod + text + colorReset);
        break;
      case "purpleBold":
        System.out.print(colorPurpleBold + text + colorReset);
      default:
        break;
    }
    if (newLine==true){
      System.out.println();
    }
      
    }
  }



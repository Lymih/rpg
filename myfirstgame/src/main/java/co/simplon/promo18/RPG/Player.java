package co.simplon.promo18.RPG;

public class Player extends Character {
  private String playerName;
  static int  nbrHealPotions;
  static int nbrRetry;
 

  public Player() {
    super("Player", 100, 100, 1, 2,2);
    this.playerName = "New player";
    Player.nbrHealPotions=20;
    Player.nbrRetry =2;
  }


  public String getPlayerName() {
    return playerName;
  }


  public void setPlayerName(String playerName) {
    this.playerName = playerName;
  }


}
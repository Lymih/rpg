package co.simplon.promo18.RPG;

import java.util.Random;

public class Fight {
  Enemy enemy;
  Game game = new Game();
  Input input = new Input();
  DisplayFeatures disp = new DisplayFeatures();
 
  char actionChoice;
 
  int limitPotionPerFight;
  Boolean limitPotionPerRound;
  int potionPossibilityStatut;

  Boolean playerTurn;


  public Fight() {
    this.limitPotionPerFight = 2;
    this.limitPotionPerRound = false;
  }

  public void fight() {
    Game.player.setCharHp(Game.player.getCharMaxHp());
    createEnemy();
    System.out.println("");
    System.out.println("Oh ! An " + enemy.getCharType() + " attacks !");
    System.out.println(enemy.getEnemyType() + " lvl " + enemy.getCharLvl());
    System.out.println("");
    playerTurn = randFirstTurn();
    while ((Game.player.getCharHp() > 0) && (enemy.getCharHp() > 0)) {
      showCharHp();
      if (playerTurn == true) {
        chooseFightAction();

        switch (actionChoice) {
          case '1':
            enemyTakeDmg();
            playerTurn = false;
            limitPotionPerRound = false;
            break;
          case '2':
            takeHealPotion();
            break;
          case '3':
            System.out.println("You run away like a coward");
            disp.printPoints(200);
            game.displayGameOver();
            break;
          default:
            System.out.println("Choice error !");
            break;
        }
      } else {
        playerTakeDmg();
        playerTurn = true;
      }

    }
    System.out.println("Fight is over !");
    if (Game.player.getCharHp() > enemy.getCharHp()) {
      System.out.println(enemy.getEnemyType() + " defeated.");
      game.levelUp();
    } else {
      System.out.println("You lost against the " + enemy.getEnemyType());
      game.askForRetry();

    }
  }

  public boolean randFirstTurn() {

    Random rand = new Random();
    boolean randedFirstTurn = rand.nextBoolean();
    if (randedFirstTurn == true) {
      System.out.println("You start first !");
    } else {
      System.out.println(enemy.getEnemyType() + " starts first !");
    }

    return randedFirstTurn;
  }

  public int playerAtks() {
    int baseAtk = rollAtk(Game.player.getCharAtk());
    int chance = rollChance();
    int dmg;
    System.out.println("You attack !");
    disp.printPoints(100);
    switch (chance) {
      case 0:
        dmg = 0;
        System.out.println("You missed !");
        break;

      case 1:
        System.out.println("You didn't hurt him a lot !");
        dmg = baseAtk / 2 - 1;
        break;

      case 5:
        dmg = baseAtk * 2;
        System.out.println("That was a huge attack !");
        break;

      default:
        dmg = baseAtk;
        break;
    }
    disp.addDelay(1000);
    System.out.println("Your attack made " + dmg + " dmg to the " + enemy.getEnemyType());
    System.out.println("");
    return dmg;
  }

  public int enemyAtks() {
    int baseAtk = rollAtk(enemy.getCharAtk());
    int chance = rollChance();
    int dmg;
    disp.pressToContinue();
    System.out.println(enemy.getEnemyType() + " attacks !");
    disp.printPoints(100);
    switch (chance) {
      case 0:
        dmg = 0;
        System.out.println(enemy.getEnemyType() + " missed !");
        break;

      case 1:
        System.out.println("He didn't hurt you a lot !");
        dmg = baseAtk / 2 - 1;
        break;

      case 5:
        dmg = baseAtk * 2;
        System.out.println("Ouch ! It hurted you a lot !");
        break;

      default:
        dmg = baseAtk;
        break;
    }
    disp.addDelay(100);
    System.out.println(enemy.getEnemyType() + " did " + dmg + " dmg to you");
    System.out.println("");
    return dmg;
  }

  public int rollAtk(int charAtk) {
    Random rand1 = new Random();
    int baseAtk = (rand1.nextInt((20 - 10) + 1) + 10) + charAtk;
    return baseAtk;
  }

  public int rollChance() {
    Random rand = new Random();
    int chance = rand.nextInt((5 - 0) + 1) + 0;
    return chance;
  }

  public void takeHealPotion() {
    int HpPotion = 50;
    int potionPos= checkPotionPossibility();
    switch (potionPos){
      case 0 :
      Player.nbrHealPotions--;
      limitPotionPerFight--;
      limitPotionPerRound = true;
      heal(HpPotion);
      break;
      case 1:
      System.out.println("You're already full hp");
      break;
      case 2:
      System.out.println("You have to wait for the next round to use an other one");
      break;
      case 3:
      System.out.println("Sorry, You can only use 2 per fight.");
      break;
      case 4:
      System.out.println("You have no more potions");
      break;
    }

   
  }
  public void heal(int HpRecovered){
    Game.player.setCharHp(Game.player.getCharHp() + HpRecovered);
      if (Game.player.getCharHp() >= Game.player.getCharMaxHp()) {
        HpRecovered = HpRecovered - (Game.player.getCharHp() - Game.player.getCharMaxHp());
        Game.player.setCharHp(Game.player.getCharMaxHp());
      }
      disp.colorText("You recovered " + HpRecovered + " HP !", "green", true);
  }

  public void chooseFightAction() {
    System.out.print("1 - Attack");
    showPotions();
    System.out.println("   3 - Run");
    actionChoice = input.scanChar();
  }

  public void showCharHp() {
    System.out.println(
        "Your HP : " + Game.player.getCharHp() + " / " + Game.player.getCharMaxHp() + "     "
            + enemy.getEnemyType() + " HP : " + enemy.getCharHp() + " / " + enemy.getCharMaxHp());
  }

  public void createEnemy() {
    enemy = new Enemy();
    enemy.setCharLvl(Game.player.getCharLvl());
    enemy.setCharAtk(enemy.getCharLvl() * 2);
    enemy.setCharDef(enemy.getCharLvl() * 2);
    enemy.setCharMaxHp(enemy.getCharMaxHp() + ((enemy.getCharLvl() - 1) * 5));
    enemy.setCharHp(enemy.getCharMaxHp());
  }

  public void enemyTakeDmg() {
    enemy.setCharHp(enemy.getCharHp() - playerAtks());
  }

  public void playerTakeDmg() {
    Game.player.setCharHp(Game.player.getCharHp() - enemyAtks());
  }

  public void showPotions() {
    System.out.print("   2 - Potion x" + Player.nbrHealPotions + "(");
    checkPotionPossibility();
    switch (potionPossibilityStatut) {
      case 0:
        disp.colorText(limitPotionPerFight + "", "green", false);
        break;
      case 1,2:
        System.out.print(limitPotionPerFight);
        break;
      case 3,4:
        disp.colorText(limitPotionPerFight + "", "red", false);
        break;
      default:
        break;
    }
    System.out.print(")");

  }

  public int checkPotionPossibility() {
    if (Player.nbrHealPotions>0){
      if (limitPotionPerFight>0){
        if (limitPotionPerRound==false){
          if (Game.player.getCharHp() <Game.player.getCharMaxHp() ){
            potionPossibilityStatut = 0;
          }else {
            potionPossibilityStatut = 1;
          }
        }else {
          potionPossibilityStatut=2;
        }
      }else {
        potionPossibilityStatut=3;

      }
    }else {
      potionPossibilityStatut=4;
    }
    return potionPossibilityStatut;
  }
}


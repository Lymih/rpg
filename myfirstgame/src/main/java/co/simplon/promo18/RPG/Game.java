package co.simplon.promo18.RPG;


public class Game {
  DisplayFeatures disp = new DisplayFeatures();
  Input input = new Input();
  static Player player;
  Fight fight;


  public void startGame() {
    System.out.println();
    System.out.println("Welcome on \"My First RPG!\"");
    disp.addDelay(1000);
    displayMainMenu();
    disp.pressToContinue();
    checkFight();
  }

  public void createPlayer() {
    player = new Player();
    askPlayerName();
    disp.printPoints(500);
    System.out.println("New player created !");
    disp.addDelay(500);
    System.out.println();
    System.out.println("Name  : " + player.getPlayerName());
    System.out.println("Level : " + player.getCharLvl());
    System.out.println(" ATK  : " + player.getCharAtk());
    System.out.println(" DEF  : " + player.getCharDef());
    System.out.println(" HP   : " + player.getCharMaxHp());
    System.out.println("");
  }

  public void displayMainMenu() {
    System.out.println();
    System.out.println("1 - Play");
    System.out.println("2 - Crédits");
    System.out.println("3 - Exit");
    chooseMenu();

  }

  public void chooseMenu() {
    char choosedMenu = input.scanChar();
    switch (choosedMenu) {

      case '1':
        System.out.println("Game");
        createPlayer();
        break;
      case '2':
        showAbout();
        displayMainMenu();
        break;
      case '3':
        System.out.println("Bye");
        System.exit(0);
        break;
      default:
        System.out.println("I didn't understand, can you repeat please ?");
        displayMainMenu();
        break;
    }
  }

  public void askReturnMenu() {
    System.out.println("Return to menu ? (You will lose your progression)");
    char choice = disp.askYesOrNo();
    switch (choice) {
      case 'y':
        disp.displayReturnMainMenu();
        displayMainMenu();
        break;
      case 'n':
        checkFight();
        break;
      default:
        System.out.println("Choice error !");
        askReturnMenu();
        break;
    }
  }

  public void askPlayerName() {
    System.out.println("Choose a name (3 to 12 characters) :");
    String playerName = input.scanString();
    if (playerName.length() < 3) {
      System.out.println("To short !");
      askPlayerName();
    } else if (playerName.length() > 12) {
      System.out.println("To long !");
      askPlayerName();
    } else {
      System.out.print("So your name is ");
      disp.colorText(playerName, "cyan", false);
      System.out.println(", right ?");
      char choice = disp.askYesOrNo();
      switch (choice) {
        case 'y':
          System.out.println("Let's go !");
          player.setPlayerName(playerName);
          break;
        case 'n':
          System.out.println("Let's choose a new one !");
          askPlayerName();
          break;
        default:
          System.out.println("I didn't get it, let's choose a new one !");
          askPlayerName();
          break;
      }

    }

  }
  public void checkFight(){
    while (player.getCharLvl() < 10 && Player.nbrRetry >= 0) {
    askForFight();
    }
    if (player.getCharLvl() == 10) {
      System.out.println("Congratulations ! You reached lvl" + player.getCharLvl());
    } else {

    }
  }
  public void askForFight() {
    System.out.println("Wanna fight ?");
    switch (disp.askYesOrNo()) {
      case 'y':
        fight = new Fight();
        fight.fight();
        break;
      case 'n':
        askReturnMenu();
        break;
      default:
        System.out.println("I didn't understand");
        askForFight();
        break;
    }

  }



  public void levelUp() {
    int pastAtk = player.getCharAtk();
    int pastDef = player.getCharDef();
    int pastMaxHp = player.getCharMaxHp();
    player.setCharLvl(player.getCharLvl() + 1);
    player.setCharAtk(player.getCharLvl() * 2);
    player.setCharDef(player.getCharLvl() * 2);
    player.setCharMaxHp(player.getCharMaxHp() + 5);
    System.out.println();
    disp.colorText("You reached lvl " + player.getCharLvl() + " !", "yellow", true);
    disp.colorText("ATK : " + player.getCharAtk() + "     (" + pastAtk + " + " + "2)", "yellow",
        true);
    disp.colorText("DEF : " + player.getCharDef() + "     (" + pastDef + " + " + "2)", "yellow",
        true);
    disp.colorText("HP  : " + player.getCharMaxHp() + "   (" + pastMaxHp + " + " + "5)", "yellow",
        true);
    System.out.println();

  }

  public void showAbout() {
    System.out.println();
    System.out.print("Title : ");
    disp.colorText("\"My First RPG\"", "blue", true);
    System.out.print("Version : ");
    disp.colorText("1.0", "blue", true);
    System.out.print("Author  : ");
    disp.colorText("Lymih", "blue", true);
    System.out.print("Year   : ");
    disp.colorText(" 2022", "blue", true);
    System.out.println();
  }

  public void askForRetry() {
    if (Player.nbrRetry > 0) {
      System.out.println("Retry ? (" + Player.nbrRetry + ")");
      char choice = disp.askYesOrNo();

      switch (choice) {
        case 'y':
          Player.nbrRetry--;
          break;
        case 'n':
          displayGameOver();
          break;
        default:
          System.out.println("I didn't understand, can you repeat please ?");
          break;
      }
    } else {
      displayGameOver();
    }
  }

  public void displayGameOver() {
    disp.addDelay(1000);
    System.out.println("Game Over");
    disp.addDelay(1000);
    disp.displayReturnMainMenu();
    displayMainMenu();
  }

}
  

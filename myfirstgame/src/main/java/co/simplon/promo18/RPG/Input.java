package co.simplon.promo18.RPG;

import java.util.Scanner;

public class Input {
  Scanner scan;

  public String scanString() {
    scan = new Scanner(System.in);
    String stringInput;
    stringInput = scan.nextLine();
    return stringInput;

  }

  public char scanChar() {
    scan = new Scanner(System.in);
    char charInput;
    try {
      charInput = scan.nextLine().charAt(0);
    } catch (Exception e) {
      charInput = 'd';
      
    }
    return charInput;
  }
}

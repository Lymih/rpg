package co.simplon.promo18.RPG;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Enemy extends Character {
  private List<String> enemyTypeList = Arrays.asList("Wolf", "Bear", "bandit", "lizard");
  private String enemyType;


  public Enemy() {
    super("Enemy", 100, 100, 1, 2, 2);
    this.enemyType = rollEnemyType();
  }

  public String rollEnemyType() {
    Random rand = new Random();
    int rolledNum = rand.nextInt((3 - 0) + 1) + 0;
    return enemyTypeList.get(rolledNum);

  }

  public String getEnemyType() {
    return enemyType;
  }

  public void setEnemyType(String enemyType) {
    this.enemyType = enemyType;
  }

}

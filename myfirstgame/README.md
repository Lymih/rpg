# Projet jeu

> ## Présentation

**Titre** : "My First RPG"

**Description** : Le jeu est une succession de combats entre un joueur (l'utilisateur) et un ennemi.
Le but du jeu est que le joueur atteigne le niveau 10. Le joueur gagne un niveau chaque fois qu'il remporte un combat.  

- Si le joueur perd un combat, il a la possibilité de recommencer 2 fois. *(retry)*
- Si le joueur n'a plus de retry ou fuit un combat, la partie est finie *(game over)*
- Plus le joueur progresse dans les niveaux et plus la difficulté augmente (les dégats infligés augmentent mais pas le montant des points de vie rendu par les potions).

L'application comporte 8 classes :
- Character
- Player
- Enemy
- Game 
- Fight
- DisplayFeatures
- Input
- Run


>## Détail des classes et de leur fonctionnement

1. ### Character

Classe mère des classes **Player** et **Enemy**.  
Elle regroupe les variables communes à ces deux sous classes *(type de personnage, points de vie, niveau, statistiques...)*. Elle comporte un constructeur qui est appelé dans ses sous classes avec des paramètres mais n'est jamais instanciée directement. Toutes les variables de cette classe sont privées. Même si l'intérêt de les déclarer dans le jeu comme tel m'échappe à l'heure actuelle, cela m'a permis de m’entraîner à utiliser les méthodes get/set.

2. ### Player

Classe enfant de **Character**.  
Le constructeur de **Player** fait appel à celui de la super classe en initialisant les variables déclarées dans **Character**. 3 nouvelles variables sont déclarées dans cette sous classe : 
- ***playerName*** (seul le joueur porte un nom)
- ***nbreHealPotions*** &  ***nbrRetry*** : 2 variables déclarées en statiques qui sont initialisées à chaque instanciation de **Player**

***playerName*** est la seule variable privée de la classe **Player**. Je ne voyais pas du tout l'intérêt de déclarer les deux autres à la fois en privées et en statiques.

3. ### Enemy

Autre classe enfant de **Character** dont elle appelle également le constructeur.  
2 nouvelles variables sont déclarées dans cette classe : ***enemyTypeList*** et ***enemyType***.  
L'enemi n'a pas de nom mais un type. A chaque instanciation de cette classe, la variable ***enemyType*** sera initialisée par un appel à la méthode ***rollEnemyType()*** qui renverra un type aléatoire d'ennemi issu de la liste **enemyTypeList**.

4. ### Game

C'est la classe principale du jeu qui commence et termine chaque partie.  
Les variables déclarées au début de cette classe sont toutes de type objet. 
C'est dans cette classe que seront instanciés les objets **Player** et **Fight**. 
Un jouer est créé à chaque début de partie, un combat est créé tant que le joueur peut combattre et qu'il n'a pas atteint le niveau 10.
Les différentes méthodes de cette classe gèrent la création d'un joueur, ses augmentations de stats en fonction du gain de niveaux, son système de retry, sa possibilité de combattre et les instanciations de ces combats (**Fight**).  

Cette classe regroupe également la majeure partie des méthodes de navigation et d'affichage *(texte et menu)*

5. ### Fight
C'est la classe la plus algorithmique du jeu.  
Elle regroupe toutes les méthodes de deal damage, de gestion des potions, des points de vie perdus ou récupérés...

#### **Fonctionnement détaillé :**
La méthode principale de cette classe est ***fight()***.
- Au début de chaque combat, les points de vie du joueur sont réinitialisés.
- Un ennemi est instancié ***(createEnemy())***.
- Le premier personnage à agir est défini de façon aléatoire ***(randFirstTurn())***.
- Tant que les 2 personnages ont leurs points de vie > 0, le combat continue. Sinon, le combat prend fin et le gagnant est celui qui a le plus de points de vie. 
- Si le joueur est vainqueur, il gagne un niveau et ses stats augmentent.

La méthode ***createEnemy()*** permet d'instancier un ennemi et d'initialiser ses stats en fonction de celles du joueur *(ils ont les mêmes)*.  
Les dégâts infligés sont calculés de façon plus ou moins aléatoire. *(Une attaque basique correspond à un Random entre 10 et 20 + l'attaque du personnage)*.  
Un random *chance* est egalement effectué permettant au personnage de réaliser :
- Soit une attaque basique
- Soit un critique *(attaque basique * 2)*
- Soit un raté *(0 dégats)*
- Soit un coup faible *(attaque basique / 2 - 1)*

Le système de gestion des potions comporte plusieurs méthodes qui vérifient et gèrent les possibilités du joueur à en consommer une (***checkPotionPossibilityStatut()***).  
Le joueur dispose de <u>20 potions par parties</u>, <u>2 potions par combat</u> et <u>1 potion par tour</u>.  

La méthode ***heal()*** est appelée pour rendre des points de vie au joueur *(pour l'instant, uniquement lorsqu'une potion est consommée)*.  
Elle fait également en sorte que ceux-ci ne dépassent pas ses points de vie max et permet d'afficher le montant exact des points de vie récupérés.

6. ### DisplayFeatures

Cette classe est liée  à l'aspect visuel du jeu dans l'invité de commande (couleurs et délai d'affichage) et comporte quelques méthodes d'interaction.  
La méthode ***colorText()*** de cette classe récupère en paramètre le texte, la couleur d'affichage et un booleen indiquant si on veut retourner à la ligne ou non à la fin de la méthode.

7. ### Input

Cette classe ne me sert qu'à regrouper les 2 méthodes de scan (String et char) que j'utilise dans le jeu. J'ai remarqué que les regrouper dans une même classe réduisait les problèmes rencontrés avec les scanners.

8. ### Run

Classe contenant la méthode ***main()*** dans laquelle j'instancie la classe Game. Elle me sert uniquement à appeler la première méthode du jeu ***(startGame())***.

> ## Défauts du jeu et améliorations à apporter

Cette version du jeu est fonctionnelle mais nécessiterait plusieurs améliorations :

- L'architecture de l'appli est à revoir.  
>- Certaines variables et méthodes ne sont pas appelées dans les classes adéquates *(je pense notamment à la méthode ***createEnemy()*** de la classe **Fight** où j'initialise des variables alors que j'aurais pu le faire directement dans le constructeur **Enemy()** ou encore les méthodes de deal damage qui aurait peut-être pu se trouver directement dans les classes de personnages.)*  
>- J'aurais voulu créer au moins une classe de plus dans cette version pour séparer un peu plus l'affichage de l'algo pur.

- Certaines variables sont sous-exploitées ou non exploitées (***charType*** n'apporte pas grand chose, la stat de def des personnages **charDef** évolue et est affichée mais n'est pas utilisée dans l'algo de combat dans la version actuelle.)

- J'ai commencé à comprendre la limite de la déclaration des variables en static même si cela ne semble pas poser de problème au fonctionnement de cette première version du jeu. J'en ai déclaré  quelques unes ainsi car cela me permettait de modifier les variables dans plusieurs classes sans avoir à les passer en paramètre à chaque fois. 

- La  *fuite* du joueur lors d'un combat est à retravailler. Dans la version actuelle, elle entraîne la fin de la partie et le retour au menu principal. J'avais pensé à créer un système de pénalité faisant baisser les stats du joueur pour un prochain combat en cas de fuite.

- Je voulais travailler un peu plus sur les couleurs et l'affichage, j'avais commencé mais je n'ai pas eu le temps de terminer pour cette version. La partie la plus élaborée du travail sur la couleur se trouve dans la gestion d'affichage des potions de la classe **Fight** (***showPotions()***)

- Je n'ai pas utilisé de test pour développer le jeu. Je ne maîtrise pas encore leur utilisation.

> ## Idées d'extensions pour de  prochaines versions

### Améliorations générales : 

- Écriture d'un scénario
- Système de commerce *(achat & vente d'objets)*
- Système de donjons


### Améliorations liées à la gestion du personnage : 
- Ajout de classes de personnages
- Ajout de caractéristiques
- Ajout de compétences
- Amélioration du système de level up *(progression par xp)*
- Gestion d'inventaire *(monnaie, consommables, équipement)*


### Nouvelle fonctionnalité :

- Sauvegarde de la progression et possibilité de charger une ou plusieurs parties sauvegardées.











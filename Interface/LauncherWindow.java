package co.simplon.promo18.Interface;

import java.awt.*;
import java.awt.event.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class LauncherWindow extends JFrame {

    // Create Window
    public LauncherWindow() {
        this.setTitle("My first RPG - Launcher");
        this.setSize(600, 600);
        this.setLocationRelativeTo(null);

        // Creat main Launcher pan
        JPanel launcher_main_pan = new JPanel();
        launcher_main_pan.setBackground(Color.BLACK);
        this.setContentPane(launcher_main_pan);

        // Creat Launcher menu pan

        JPanel launcher_menu = new JPanel();
        launcher_menu.setSize(200, 200);
        launcher_menu.setLayout(new GridLayout(3, 1));
        launcher_main_pan.add(launcher_menu);

        JButton start_button = new JButton("Start to play");
        JButton about_button = new JButton("About");
        JButton exit_button = new JButton("Exit");

        start_button.addActionListener(new ClickOnStart());
        // start_button.addActionListener(new ClickOnExit());

        launcher_menu.add(start_button);
        launcher_menu.add(about_button);
        launcher_menu.add(exit_button);

        this.setVisible(true);

    }

    public class ClickOnStart implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            dispose();
            CreatCharacterWindow new_game_window = new CreatCharacterWindow();

        }

    }

      public class ClickOnExit implements ActionListener {
      
      @Override
      public void actionPerformed(ActionEvent e) {
          dispose();
      
      
      }
      
     }

}
